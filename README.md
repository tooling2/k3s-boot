# K3s Boot

Helping you to start, stop and reser your k3s

```
Helping you to start, stop and reser your k3s
Usage: /home/jdelacamara/.local/bin/k3s-boot [--(no-)quiet] [-s|--service <arg>] [-d|--dir <arg>] [-k|--kubeconfigfile <arg>] [--(no-)force-start] [--(no-)deploy-traefik] [-h|--help] [<verb>]
	<verb>: What to do, one of start, stop, reset (stop, delete config, fresh start), getkc (get kubeconfig to a file), check; default is check (default: 'check')
	--quiet, --no-quiet: shhh, be quiet (off by default)
	-s, --service: The service name, default k3s.service  (default: 'k3s.service')
	-d, --dir: The directory name, default /var/lib/rancher  (default: '/var/lib/rancher')
	-k, --kubeconfigfile: The kubeconfig file name, default k3s-kubeconfig  (default: 'k3s-kubeconfig')
	--force-start, --no-force-start: After resetting start the service, even if it was not running in first place (off by default)
	--deploy-traefik, --no-deploy-traefik: When starting do not deploy Traefix ingress (on by default)
	-h, --help: Prints help
```

## What?

Just that, a script that helps you to start, stop and reset the k3s cluster on your PC.

Actually, the first thing I needed, was a fast way to reset my cluster (meaning, deleting all the confs, setting it as in a fresh start, without uninstalling and reinstalling it). Then, I added the start and stop... and then the get-kubeconfig-file...

## Pre requisite

A req, at least for this version, is that k3s is working through systemd. But this script can be improved to work in other ways. (no, stop there, it won't prepare your coffee)

## Quick start

Download it. Run it:

```
k3s-boot start
```

voilà!

(I wanna to thanks Carlitos Balá for make my chilhood happier!)

## Modify the script

[Argbash](https://argbash.dev/) is being used. So for changing the script:

- modify the `k3s-boot.m4` file
- run `argbash k3s-boot.m4 > k3s-boot.sh`
- enjoy it
